﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TextEditor.Abstract
{
    public interface ILogInViewModel
    {
        string UserName { get; set; }
        void LogIn(string username);
        void CloseWindow(Window window);
    }
}
