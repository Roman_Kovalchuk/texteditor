﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TextEditor.Abstract
{
    public interface ITextEditorViewModel
    {
        string FilePath { get; set; }
        void Exit();
        void Save(string text);
        string Open();
        string WaitForFreeFile(Func<string, bool> IsFileUsing);
    }
}
