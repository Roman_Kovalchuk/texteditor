﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using TextEditor.Models;
using TextEditor.Properties;

namespace TextEditor.Infrastructure
{
    class UsersAndFilesCollection
    {
        private readonly string binaryFilePath = @"serialized.bin";

        public List<UserAndFilePair> UsersAndFiles
        {
            get
            {
                using (var stream = File.Open(binaryFilePath, FileMode.OpenOrCreate))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    try
                    {
                        List<UserAndFilePair> usersAndFiles = JsonConvert.DeserializeObject<List<UserAndFilePair>>((formatter.Deserialize(stream) as string));
                        stream.Close();
                        return usersAndFiles;
                    }
                    catch
                    {
                        stream.Close();
                        UsersAndFiles = new List<UserAndFilePair>();
                        return UsersAndFiles;
                    }
                }
            }
            private set
            {
                using (var stream = File.Open(binaryFilePath, FileMode.OpenOrCreate))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, JsonConvert.SerializeObject(value));
                    stream.Close();
                }
            }

        }
        public void AddUserAndFile(UserAndFilePair userAndFile)
        {
            var usersAndFiles = UsersAndFiles;
            usersAndFiles.Add(userAndFile);
            UsersAndFiles = usersAndFiles;
        }
        public void RemoveUserAndFile(string userName)
        {
            var usersAndFiles = UsersAndFiles;
            usersAndFiles.RemoveAll(x=>x.UserName==userName);
            UsersAndFiles = usersAndFiles;
        }

        public bool IsFileUsing(string fileName)
        {
            return UsersAndFiles.Any(x => x.FileName == fileName);
        }


    }
}
