﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextEditor.Models
{
    class UserAndFilePair
    {
        public string UserName { get; set; }
        public string FileName { get; set; }
    }
}
