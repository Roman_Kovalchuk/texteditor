﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TextEditor.Abstract;

namespace TextEditor.ViewModels
{
    class LogInViewModel : ILogInViewModel
    {
        public string UserName { get; set; }

        public void CloseWindow(Window window)
        {
            if (string.IsNullOrWhiteSpace(UserName))
            {
                Application.Current.Shutdown();
            }
            else
            {
                window.Close();
            }
        }

        public void LogIn(string userName)
        {
            UserName = userName;
            MessageBox.Show($"You was logged as {UserName}!");
        }
    }
}
