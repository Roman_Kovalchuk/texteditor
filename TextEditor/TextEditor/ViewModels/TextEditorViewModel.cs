﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using TextEditor.Abstract;

namespace TextEditor.ViewModels
{
    class TextEditorViewModel : ITextEditorViewModel
    {
        public string FilePath { get; set; }

        public void Exit()
        {
            Application.Current.Shutdown();
        }

        public void Save(string text)
        {
            if (string.IsNullOrWhiteSpace(FilePath))
            {
                throw new Exception("You didn`t choose file yet!");
            }
            using (StreamWriter sw = new StreamWriter(FilePath))
            {
                sw.Write(text);
                sw.Close();
            }
            MessageBox.Show("File was updated!");
        }

        public string Open()
        {
            ChooseFile();
            return GetTextFromFile();
        }

        public string WaitForFreeFile(Func<string, bool> IsFileUsing)
        {
            while (IsFileUsing(FilePath))
            {
                Thread.Sleep(10000);
            }
            return GetTextFromFile();
        }

        private string GetTextFromFile()
        {
            StringBuilder fileText = new StringBuilder();
            using (StreamReader sr = new StreamReader(FilePath))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    fileText.Append(line);
                }
                sr.Close();
            }
            return fileText.ToString();
        }

        private void ChooseFile()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.DefaultExt = ".txt";
            if (dialog.ShowDialog().Value != true)
            {
                throw new Exception("File problem!");
            }
            FilePath = dialog.FileName;
        }
    }
}
