﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TextEditor.Abstract;

namespace TextEditor.Views
{
    /// <summary>
    /// Interaction logic for LogInWindow.xaml
    /// </summary>
    public partial class LogInWindow : Window
    {
        private ILogInViewModel _viewModel;
        public LogInWindow(ILogInViewModel viewModel)
        {
            InitializeComponent();
            _viewModel = viewModel;
            Closed += CloseWindow;
        }

        private void LogIn(object sender, RoutedEventArgs e)
        {
            try
            {
                _viewModel.LogIn(Name.Text);
                _viewModel.CloseWindow(this);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void CloseWindow(object sender, EventArgs e)
        {
            try
            {
                _viewModel.CloseWindow(this);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }
    }
}
