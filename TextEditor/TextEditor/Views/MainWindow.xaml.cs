﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TextEditor.Abstract;
using TextEditor.Infrastructure;
using TextEditor.ViewModels;
using TextEditor.Views;

namespace TextEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ITextEditorViewModel _viewModel;
        private ILogInViewModel _logInViewModel;
        private UsersAndFilesCollection usersAndFiles = new UsersAndFilesCollection();
        public MainWindow(ITextEditorViewModel viewModel, ILogInViewModel logInViewModel)
        {
            InitializeComponent();
            InitializeViewModels(viewModel, logInViewModel);
            InitializeWindow();
        }

        private void InitializeWindow()
        {
            DisableEditingText();
            Loaded += LogIn;
            Closed += (x, y) => RemoveUserFromHistory();
        }

        private void InitializeViewModels(ITextEditorViewModel viewModel, ILogInViewModel logInViewModel)
        {
            _viewModel = viewModel;
            DataContext = _viewModel;
            _logInViewModel = logInViewModel;
        }

        private void LogIn(object sender, RoutedEventArgs e)
        {
            var logInWindow = new LogInWindow(_logInViewModel);
            try
            {
                logInWindow.Show();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            RemoveUserFromHistory();
            _viewModel.Exit();
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            try
            {
                TextRange textRange = new TextRange(FileText.Document.ContentStart, FileText.Document.ContentEnd);
                _viewModel.Save(textRange.Text);
                _viewModel.FilePath = null;
                ClearEditor();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                RemoveUserFromHistory();
            }

        }

        private void ClearEditor()
        {
            FileText.Document.Blocks.Clear();
            DisableEditingText();
        }

        private void Open(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearEditor();
                FileText.AppendText(_viewModel.Open());
                if (!usersAndFiles.IsFileUsing(_viewModel.FilePath))
                {
                    FileText.IsEnabled = true;
                    AddUserFromHistory();
                }
                else
                {
                    MessageBox.Show("This file is using by another user!");
                    Dispatcher.Invoke(() =>
                        {
                            string text = _viewModel.WaitForFreeFile(usersAndFiles.IsFileUsing);
                            AddUserFromHistory();
                            ClearEditor();
                            MessageBox.Show("You can edit this file now!");
                            FileText.AppendText(text);
                            FileText.IsEnabled = true;
                        });

                }

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

        }
        private void RemoveUserFromHistory()
        {
            usersAndFiles.RemoveUserAndFile(_logInViewModel.UserName);
        }

        private void AddUserFromHistory()
        {
            usersAndFiles.AddUserAndFile(new Models.UserAndFilePair
            {
                FileName = _viewModel.FilePath,
                UserName = _logInViewModel.UserName
            });
        }

        private void SaveAndUpdate(object sender, RoutedEventArgs e)
        {
            this.Save(sender, e);
            this.Exit(sender, e);
        }
        private void DisableEditingText()
        {
            FileText.IsEnabled = false;
        }
    }
}
